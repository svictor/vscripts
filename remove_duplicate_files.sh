#!/bin/bash
# This will scan current dir and subdirs looking for duplicates
# Duplicates will be sent to trash

declare -A arr
shopt -s globstar

for file in **; do
  [[ -f "$file" ]] || continue

  # First < is redirect, then comes <(cmd) construct which is command
  # substitution
  # Using is a way to discard subsequent words from the input
  read cksm _ < <(md5sum "$file")
  # cksm is stored as label. Value doesn’t matter
  if ((arr[$cksm]++)); then 
    echo "mv $file ~/.local/share/Trash/files/"
  fi
done
