Scripts locaux à `~/.local/bin/vscripts`. Fonctionnent avec les [unités systemd](https://framagit.org/svictor/systemd_units)

Les liens symboliques `recevoir_mails` et `envoyer_mails` dans ce répertoire sont hors de git. 